[org 0x7c00]
mov [BOOT_DISK], dl
mov bp, 0x7c00
mov sp, bp
call ReadDisk
pusha
mov ax, 0x0700
mov bh, 0x07
mov cx, 0x0000
mov dx, 0x184f
int 0x10
popa
mov ah, 0x02
mov bh, 0
mov dh, 0
mov dl, 0
int 0x10
jmp PROGRAM_SPACE
PrintString:
	push ax
	push bx	
	mov ah, 0x0e
	.Loop:
	cmp [bx], byte 0
	je .Exit
		mov al, [bx]
		int 0x10
		inc bx
		jmp .Loop
	.Exit:	
	pop ax
	pop bx
	ret
PROGRAM_SPACE equ 0x7e00
ReadDisk:
	mov ah, 0x02
	mov bx, PROGRAM_SPACE
	mov al, 64
	mov dl, [BOOT_DISK]
	mov ch, 0x00
	mov dh, 0x00
	mov cl, 0x02
	int 0x13
	jc DiskReadFailed
	ret
BOOT_DISK:
	db 0
DiskReadErrorString:
	db 'Disk Read Failed',0
DiskReadFailed:
	mov bx, DiskReadErrorString
	call PrintString
	jmp $
times 510-($-$$) db 0
dw 0xaa55
push bx
push ax
mov bx, str0
call PrintString
mov ah, 0x0e
mov al, 10
int 0x10
mov al, 13
int 0x10
pop ax
pop bx
in al, 0x92
or al, 2
out 0x92, al
cli
lgdt [gdt_descriptor]
mov eax, cr0
or eax, 1
mov cr0, eax
jmp codeseg:main
[bits 32]
main:
	mov eax,  0xb8000 
	mov cl,  'M' 
	mov [eax],  cl 
	inc eax
	inc eax
	ret
str0: db 'Hello, World!', 0
jmp $
gdt_nulldesc:
	dd 0
	dd 0	
gdt_codedesc:
	dw 0xFFFF
	dw 0x0000			; Base (low)
	db 0x00				; Base (medium)
	db 10011010b		; Flags
	db 11001111b		; Flags + Upper Limit
	db 0x00				; Base (high)
gdt_datadesc:
	dw 0xFFFF
	dw 0x0000
	db 0x00
	db 10010010b
	db 11001111b
	db 0x00
gdt_end:
gdt_descriptor:
	gdt_size: 
		dw gdt_end - gdt_nulldesc - 1
		dq gdt_nulldesc
codeseg equ gdt_codedesc - gdt_nulldesc
dataseg equ gdt_datadesc - gdt_nulldesc
[bits 32]
EditGDT:
	mov [gdt_codedesc + 6], byte 10101111b
	mov [gdt_datadesc + 6], byte 10101111b
	ret