package osm;

import static osm.assembly.Boot.call;
import static osm.assembly.Boot.cls;
import static osm.assembly.Boot.getBootOrg;
import static osm.assembly.Boot.jmp;
import static osm.assembly.Boot.mov;
import static osm.assembly.Boot.printFunc;
import static osm.assembly.Boot.setAsBoot;
import static osm.assembly.Kernel.parseFile;

import java.io.File;
import java.io.FileWriter;

import osm.util.argument.Argument;
import osm.util.argument.ArgumentUtil;

/**
 * Main.java
 * 
 * Author:  *********** ***
 * Project: OSM
 * Licence: (none)
 * Created: 28-11-2021
 * Updated: 03-12-2021
 * Description: Main class of the project
 */
public class Main {
	public static void main(String[] args) throws Exception {
		Argument kernelFile = ArgumentUtil.getOptionalArg(args, "kernel");
		String asmFileKeep = ArgumentUtil.getRequiredArg(args, "keep-asm-file");
		String outputFleType = ArgumentUtil.getRequiredArg(args, "f");
		
		File out2 = new File("os.flp");
		if(out2.exists()) out2.delete();
		
		File out = new File("boot.asm");
		if(out.exists()) out.delete();
		out.createNewFile();
		
		String code = getBootOrg() + "\n" + mov("[BOOT_DISK]", "dl") + "\n" + mov("bp", "0x7c00") + "\n" + mov("sp", "bp") + "\n" + call("ReadDisk") + "\n" + cls() + "\n" + jmp("PROGRAM_SPACE") + "\n" + printFunc() + "\n" + diskRead() + "\n" + setAsBoot() + "\n" + parseFile(kernelFile.doesExist() ? kernelFile.getValue() : "kernel.osm");
		FileWriter writer = new FileWriter(out);
		writer.write(code);
		writer.close();
		Process p = Runtime.getRuntime().exec("nasm boot.asm -f bin -o os.flp");
		while(p.isAlive());
		System.out.println("OS compiling exited with " + p.exitValue());
		out2.deleteOnExit();
		
		if(outputFleType.equals("iso")) {
			File dir = new File("isotmp"); if(!dir.exists()) dir.mkdir();
			p = Runtime.getRuntime().exec("dd if=/dev/zero of=isotmp/floppy.img bs=1024 count=2880");
			while(p.isAlive());
			System.out.println(p.exitValue());
			p = Runtime.getRuntime().exec("dd if=os.flp of=isotmp/floppy.img seek=0 count=4 conv=notrunc");
			while(p.isAlive());
			System.out.println(p.exitValue());
			p = Runtime.getRuntime().exec("genisoimage -quiet -V 'MYOS' -input-charset iso8859-1 -o os.iso -b floppy.img -hide floppy.img isotmp/");
			while(p.isAlive());
			System.out.println(p.exitValue());
		}else if(outputFleType.equals("floppy")) {
			
		}else {
			System.err.println("Unknown file output type \"" + outputFleType + "\"!");
		}
		
		if(!Boolean.parseBoolean(asmFileKeep)) out.delete();
	}
	
	public static String diskRead() {
		return "PROGRAM_SPACE equ 0x7e00\nReadDisk:\n\tmov ah, 0x02\n\tmov bx, PROGRAM_SPACE\n\tmov al, 64\n\tmov dl, [BOOT_DISK]\n\tmov ch, 0x00\n\tmov dh, 0x00\n\tmov cl, 0x02\n\tint 0x13\n\tjc DiskReadFailed\n\tret\nBOOT_DISK:\n\tdb 0\nDiskReadErrorString:\n\tdb 'Disk Read Failed',0\nDiskReadFailed:\n\tmov bx, DiskReadErrorString\n\tcall PrintString\n\tjmp $";
	}
}
