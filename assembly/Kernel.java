package osm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;

import osm.exception.TypeNotFoundException;

/**
 * Kernel.java
 * 
 * Author:  *********** ***
 * Project: OSM
 * Licence: (none)
 * Created: 28-11-2021
 * Updated: 02-12-2021
 * Description: .osm kernel to asm converter
 */
public class Kernel {
	private static boolean dontadd = false, function = false, bit32modeused = false, cls = false;
	private static int linen = 0;
	private static HashMap<Integer, String> strings = new HashMap<Integer, String>();
	public static String parseFile(String file) throws Exception {
		String result = "";
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(file))));
		String line;
		while((line = reader.readLine()) != null) {
			linen++;
			if(line.equals("")) continue;
			line = line.replaceAll("	", " ");
			String[] tokens = line.split(" ");
			result += switchc(tokens);
			if(!dontadd) result += "\n";
			dontadd=false;
		}
		for(int i = 0; i < strings.size(); i++) {
			result += ("str" + i + ": db '" + strings.get(i) + "', 0\n");
		}
		result += "jmp $\n";
//		result += "[bits 64]\nVGA_MEMORY equ 0xB8000\nVGA_WIDTH equ 80\nPrintString64bit:\n  mov rax, VGA_MEMORY\n  .Loop:\n    cmp [rbx], byte 0\n    je .Exit\n     \n    mov cl, [rbx]\n    mov [rax], cl    inc rax\n    inc rax\n    inc rbx\n    jmp .Loop\n  .Exit:\n   ret\n";
//		result += "PageTableEntry equ 0x1000SetUpIdentityPaging:\n\tmov edi, PageTableEntry\n\tmov cr3, edi\tmov dword [edi], 0x2003\n\tadd edi, 0x1000\n\tmov dword [edi], 0x3003\n\tadd edi, 0x1000\n\tmov dword [edi], 0x4003\n\tadd edi, 0x1000\tmov ebx, 0x00000003\n\tmov ecx, 512\t.SetEntry:\n\t\tmov dword [edi], ebx\n\t\tadd ebx, 0x1000\n\t\tadd edi, 8\n\t\tloop .SetEntry\tmov eax, cr4\n\tor eax, 1 << 5\n\tmov cr4, eax\tmov ecx, 0xC0000080\n\trdmsr\n\tor eax, 1 << 8\n\twrmsr\tmov eax, cr0\n\tor eax, 1 << 31\n\tmov cr0, eax\tret\n";
//		result += "[bits 32]\nDetectCPUID:\n\tpushfd \n\tpop eax\t\n\tmov ecx, eax\t\n\txor eax, 1 << 21\t\n\tpush eax\n\tpopfd\n\tpushfd \n\tpop eax\t\n\tpush ecx \n\tpopfd\t\n\txor eax,ecx\n\tjz NoCPUID\n\tret\nDetectLongMode:\n\tmov eax, 0x80000001\n\tcpuid\n\ttest edx, 1 << 29\n\tjz NoLongMode\n\tret\nNoLongMode:\n  mov [0xb8000], byte 'N'\n\tmov [0xb8002], byte 'L'\n\tmov [0xb8004], byte 'M'\n\tmov [0xb8006], byte 'S'\n\thlt\nNoCPUID:\n  mov [0xb8000], byte 'N'\n\tmov [0xb8002], byte 'C'\n\tmov [0xb8004], byte 'I'\n\tmov [0xb8006], byte 'S'\n\thlt\n[bits 64]\nVGA_MEMORY equ 0xB8000\nVGA_WIDTH equ 80\nPrintString64bit:\n  mov rax, VGA_MEMORY\n  .Loop:\n    cmp [rbx], byte 0\n    je .Exit\n    mov cl, [rbx]\n    mov [rax], cl    \n    inc rax\n    inc rax\n    inc rbx\n    jmp .Loop\n  .Exit:\n   ret\n[bits 16]\nPageTableEntry equ 0x1000\nSetUpIdentityPaging:\n\tmov edi, PageTableEntry\n\tmov cr3, edi\n\tmov dword [edi], 0x2003\n\tadd edi, 0x1000\n\tmov dword [edi], 0x3003\n\tadd edi, 0x1000\n\tmov dword [edi], 0x4003\n\tadd edi, 0x1000\n\tmov ebx, 0x00000003\n\tmov ecx, 512\n\t.SetEntry:\n\t\tmov dword [edi], ebx\n\t\tadd ebx, 0x1000\n\t\tadd edi, 8\n\t\tloop .SetEntry\n\tmov eax, cr4\n\tor eax, 1 << 5\n\tmov cr4, eax\n\tmov ecx, 0xC0000080\n\trdmsr\n\tor eax, 1 << 8\n\twrmsr\n\tmov eax, cr0\n\tor eax, 1 << 31\n\tmov cr0, eax\n\tret";
		if(bit32modeused)
			result += "gdt_nulldesc:\n\tdd 0\n\tdd 0\t\ngdt_codedesc:\n\tdw 0xFFFF\n\tdw 0x0000\t\t\t; Base (low)\n\tdb 0x00\t\t\t\t; Base (medium)\n\tdb 10011010b\t\t; Flags\n\tdb 11001111b\t\t; Flags + Upper Limit\n\tdb 0x00\t\t\t\t; Base (high)\ngdt_datadesc:\n\tdw 0xFFFF\n\tdw 0x0000\n\tdb 0x00\n\tdb 10010010b\n\tdb 11001111b\n\tdb 0x00\ngdt_end:\ngdt_descriptor:\n\tgdt_size: \n\t\tdw gdt_end - gdt_nulldesc - 1\n\t\tdq gdt_nulldesc\ncodeseg equ gdt_codedesc - gdt_nulldesc\ndataseg equ gdt_datadesc - gdt_nulldesc\n[bits 32]\nEditGDT:\n\tmov [gdt_codedesc + 6], byte 10101111b\n\tmov [gdt_datadesc + 6], byte 10101111b\n\tret";
		if(cls)
			result += "clearScreen:\n    pusha\n    mov ax, 0x0700\n    mov bh, 0x07\n    mov cx, 0x0000\n    mov dx, 0x184f\n    int 0x10\n    popa\n    ret";
		return result;
	}
	
	static String switchc(String[] tokens) {
		String result = "";
		String string = "";
		try {
			switch(tokens[0]) {
				case "func":
					function = true;
					result += tokens[1] + ":";
					break;
				case "func_end":
					result += "\tret";
					function = false;
					break;
				case "halt":
					result += (function ? "	" : "") + "hlt";
					break;
				case "call":
					result += ((function ? "	" : "") + "call ") + tokens[1];
					break;
				case "print":
					for(int i = 1; i < tokens.length; i++) {
						string += tokens[i];
						if(string.startsWith("\"")) string = string.substring(1, string.length());
						if(string.endsWith("\"")) { string = string.substring(0, string.length() - 1); break; }
						string += " ";
					}
					
					strings.put(strings.size(), string);
					
					result += ((function ? "	" : "") + "push bx\n");
					result += ((function ? "	" : "") + "mov bx, str" + (strings.size() - 1)) + "\n";
					result += ((function ? "	" : "") + "call PrintString");
					result += ((function ? "	" : "") + "pop bx");
					break;
				case "println":
					for(int i = 1; i < tokens.length; i++) {
						string += tokens[i];
						if(string.startsWith("\"")) string = string.substring(1, string.length());
						if(string.endsWith("\"")) { string = string.substring(0, string.length() - 1); break; }
						string += " ";
					}
					
					strings.put(strings.size(), string);
					
					result += ((function ? "	" : "") + "push bx\n");
					result += ((function ? "	" : "") + "push ax\n");
					result += ((function ? "	" : "") + "mov bx, str" + (strings.size() - 1)) + "\n";
					result += ((function ? "	" : "") + "call PrintString\n");
					result += ((function ? "	" : "") + "mov ah, 0x0e\n");
					result += ((function ? "	" : "") + "mov al, 10\n");
					result += ((function ? "	" : "") + "int 0x10\n");
					result += ((function ? "	" : "") + "mov al, 13\n");
					result += ((function ? "	" : "") + "int 0x10\n");
					result += ((function ? "	" : "") + "pop ax\n");
					result += ((function ? "	" : "") + "pop bx");
					break;
				case "bits":
					result += ((function ? "	" : "") + "[bits " + tokens[1] + "]");
					break;
				case "load32Bit":
					bit32modeused = true;
					result += "in al, 0x92\nor al, 2\nout 0x92, al\ncli\nlgdt [gdt_descriptor]\nmov eax, cr0\nor eax, 1\nmov cr0, eax\njmp codeseg:" + tokens[1] + "\n[bits 32]";
					break;
				case "move":
					for(int i = 1; i < tokens.length; i++) {
						string += tokens[i];
						string += " ";
					}
					result += ((function ? "	" : "") + "mov " + string.split(",")[0] + ", " + string.split(",")[1]);
					break;
				case "print64":
	//				for(int i = 1; i < tokens.length; i++) {
	//					string += tokens[i];
	//					if(string.startsWith("\"")) string = string.substring(1, string.length());
	//					if(string.endsWith("\"")) { string = string.substring(0, string.length() - 1); break; }
	//					string += " ";
	//				}
	//				
	//				strings.put(strings.size(), string);
	//				
	//				result += ((function ? "	" : "") + "push rbx\n");
	//				result += ((function ? "	" : "") + "push rax\n");
	//				result += ((function ? "	" : "") + "push cl\n");
	//				result += ((function ? "	" : "") + "mov rbx, str" + (strings.size() - 1)) + "\n";
	//				result += ((function ? "	" : "") + "call PrintString64bit\n");
	//				result += ((function ? "	" : "") + "mov ah, 0x0e\n");
	//				result += ((function ? "	" : "") + "mov al, 10\n");
	//				result += ((function ? "	" : "") + "int 0x10\n");
	//				result += ((function ? "	" : "") + "mov al, 13\n");
	//				result += ((function ? "	" : "") + "int 0x10\n");
	//				result += ((function ? "	" : "") + "pop rax\n");
	//				result += ((function ? "	" : "") + "pop rbx");
	//				result += ((function ? "	" : "") + "pop cl\n");
	//				break;
					
					throw new RuntimeException("Error at line " + linen + ": Not Supported!");
				case "println64":
	//				for(int i = 1; i < tokens.length; i++) {
	//					string += tokens[i];
	//					if(string.startsWith("\"")) string = string.substring(1, string.length());
	//					if(string.endsWith("\"")) { string = string.substring(0, string.length() - 1); break; }
	//					string += " ";
	//				}
	//				
	//				strings.put(strings.size(), string);
	//				
	//				result += ((function ? "	" : "") + "push rbx\n");
	//				result += ((function ? "	" : "") + "push rax\n");
	//				result += ((function ? "	" : "") + "push cl\n");
	//				result += ((function ? "	" : "") + "mov rbx, str" + (strings.size() - 1)) + "\n";
	//				result += ((function ? "	" : "") + "call PrintString64bit\n");
	//				result += ((function ? "	" : "") + "mov ah, 0x0e\n");
	//				result += ((function ? "	" : "") + "mov al, 10\n");
	//				result += ((function ? "	" : "") + "int 0x10\n");
	//				result += ((function ? "	" : "") + "mov al, 13\n");
	//				result += ((function ? "	" : "") + "int 0x10\n");
	//				result += ((function ? "	" : "") + "pop rax\n");
	//				result += ((function ? "	" : "") + "pop rbx\n");
	//				result += ((function ? "	" : "") + "pop cl");
	//				break;
					
					throw new RuntimeException("Error at line " + linen + ": Not Supported!");
				case "load64bit":
	//				result += (function ? "	" : "") + "call DetectCPUID\n";
	//				result += (function ? "	" : "") + "mov [0xb8004], byte \"c\"\n";
	//				result += (function ? "	" : "") + "call DetectLongMode\n";
	//				result += (function ? "	" : "") + "mov [0xb8006], byte \"k\"\n";
	//				result += (function ? "	" : "") + "call SetUpIdentityPaging\n";
	//				result += (function ? "	" : "") + "mov [0xb8008], byte \" \"\n";
	//				result += (function ? "	" : "") + "call EditGDT\n";
	//				result += (function ? "	" : "") + "mov [0xb8004], byte \"U\"\n";
	//				result += (function ? "	" : "") + "jmp codeseg:" + tokens[1] + "";
	//				break;
					
					throw new RuntimeException("Error at line " + linen + ": Not Supported!");
				case ";": dontadd=true; break;
				case "": result += switchc(leftRotatebyOne(tokens)); break;
				case "jmp" :
					result += (function ? "	" : "") + "jmp " + tokens[1] + "";
					break;
				case "cls":
					cls = true;
					result += "call clearScreen";
					break;
				case "int":
					result += "int " + tokens[1];
					break;
				case "var":
					for(int i = 2; i < tokens.length; i++) {
						string += tokens[i];
						if(string.startsWith("\"")) string = string.substring(1, string.length());
						if(string.endsWith("\"")) { string = string.substring(0, string.length() - 1); break; }
						string += " ";
					}
					result += (function ? "	" : "") + tokens[1] + ": db " + string;
					break;
				case "inc":
					for(int i = 2; i < tokens.length; i++) {
						string += tokens[i];
						string += " ";
					}
					result += (function ? "	" : "") + "inc " + tokens[1];
					break;
				default:
					throw new TypeNotFoundException("Unknown token " + tokens[0] + ":" + linen + "!");
			}
		} catch(Exception e) {
			System.err.println("Exception at line " + linen + "!");
			e.printStackTrace();
			System.exit(-1);
		}
		return result;
	}
	private static String[] leftRotatebyOne(String arr[])
	{
		int n = arr.length;
		String temp[] = arr;
	    int i;
	    for (i = 0; i < n - 1; i++) temp[i] = arr[i + 1];
	    return Arrays.copyOf(temp, temp.length-1);
	}
}
