package osm;

/**
 * Boot.java
 * 
 * Author:  *********** ***
 * Project: OSM
 * Licence: (none)
 * Created: 28-11-2021
 * Updated: 02-12-2021
 * Description: Boot functions in assembly
 */
public class Boot {
	public static String getBootOrg() {
		return "[org 0x7c00]";
	}
	public static String mov(String a, String b) {
		return "mov " + a + ", " + b;
	}
	public static String setAsBoot() {
		return "times 510-($-$$) db 0\ndw 0xaa55";
	}
	public static String jmp(String jmp) {
		return "jmp " + jmp;
	}
	public static String call(String func) {
		return "call " + func;
	}
	public static String equ(String var, String val) {
		return var + " equ " + val;
	}
	public static String printFunc() {
		return "PrintString:\n\tpush ax\n\tpush bx\t\n\tmov ah, 0x0e\n\t.Loop:\n\tcmp [bx], byte 0\n\tje .Exit\n\t\tmov al, [bx]\n\t\tint 0x10\n\t\tinc bx\n\t\tjmp .Loop\n\t.Exit:\t\n\tpop ax\n\tpop bx\n\tret";
	}
	public static String string(String var, String val) {
		return var + ": db '" + val + "', 0";
	}
	public static String emptyVar(String var) {
		return var + ": db 0";
	}
	public static String cls() {
		return "pusha\nmov ax, 0x0700\nmov bh, 0x07\nmov cx, 0x0000\nmov dx, 0x184f\nint 0x10\npopa\nmov ah, 0x02\nmov bh, 0\nmov dh, 0\nmov dl, 0\nint 0x10";
	}
}
