package osm.exception;

/**
 * ArgumentUtil.java
 * 
 * Author:  *********** ***
 * Project: OSM
 * Licence: (none)
 * Created: 28-11-2021
 * Updated: 02-12-2021
 * Description: Exception thrown when a require argument was not found
 */
public class RequiredArgNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 2434357972199559430L;
	
	public RequiredArgNotFoundException(String string) {
		super(string);
	}
}
